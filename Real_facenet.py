from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from scipy import misc
import cv2
import numpy as np
import facenet
import detect_face
import os
import time
import pickle
import json


class ImportGraph():
    """  Importing and running isolated TF graph """
    def __init__(self, loc):
        # Create local graph and use it in the session
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)
        with self.graph.as_default():
            # Import saved model from location 'loc' into local graph
            saver = tf.train.import_meta_graph(loc + '.meta',
                                               clear_devices=True)
            saver.restore(self.sess, loc)
            self.x = self.graph.get_tensor_by_name("Placeholder:0")
            self.hold_prob1 = self.graph.get_tensor_by_name("Placeholder_2:0")
            self.hold_prob2 = self.graph.get_tensor_by_name("Placeholder_3:0")
            self.y_pred = self.graph.get_tensor_by_name("add_7:0")


    def run(self, data):
        """ Running the activation operation previously imported """
        return self.sess.run([tf.nn.softmax(self.y_pred)], feed_dict={self.x: data, self.hold_prob1: 1, self.hold_prob2: 1})


model_1 = ImportGraph('models/liveness detection/model.ckpt')
print("model 1 loaded")

cascPath = "haarcascade_frontalface_default.xml"

# Create the haar cascade
faceCascade = cv2.CascadeClassifier(cascPath)

print('Creating networks and loading parameters')
with tf.Graph().as_default():
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
    with sess.as_default():
        pnet, rnet, onet = detect_face.create_mtcnn(sess, os.getcwd() + '/align')

        minsize = 100  # minimum size of face
        threshold = [0.8, 0.8, 0.92]  # three steps's threshold
        factor = 0.709  # scale factor
        margin = 44
        frame_interval = 1
        batch_size = 1000
        image_size = 170
        input_image_size = 160

        with open('data.txt') as json_file:
            HumanNames = json.load(json_file)

        print('Loading feature extraction model')
        modeldir = os.getcwd() + '/models/facenet/20170512-110547.pb'
        facenet.load_model(modeldir)

        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        embedding_size = embeddings.get_shape()[1]

        classifier_filename = os.getcwd() + '/models/classifier/classifier.pkl'
        classifier_filename_exp = os.path.expanduser(classifier_filename)
        with open(classifier_filename_exp, 'rb') as infile:
            (model, class_names) = pickle.load(infile)
            print('load classifier file-> %s' % classifier_filename_exp)

        video_capture = cv2.VideoCapture(0)
        c = 0

        print('Start Recognition!')
        prevTime = 0

        # video writer
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        out = cv2.VideoWriter('demoVideo.avi', fourcc, fps=5, frameSize=(640, 480))

        while True:
            ret, frame = video_capture.read()
            image = frame
            # frame = cv2.resize(frame, (0,0), fx=0.5, fy=0.5)    #resize frame (optional)

            curTime = time.time()    # calc fps
            timeF = frame_interval

            if (c % timeF == 0):
                find_results = []

                if frame.ndim == 2:
                    frame = facenet.to_rgb(frame)
                frame = frame[:, :, 0:3]
                bounding_boxes, _ = detect_face.detect_face(frame, minsize, pnet, rnet, onet, threshold, factor)
                nrof_faces = bounding_boxes.shape[0]
                print('Detected_FaceNum: %d' % nrof_faces)

                if nrof_faces > 0:
                    det = bounding_boxes[:, 0:4]
                    img_size = np.asarray(frame.shape)[0:2]

                    cropped = []
                    scaled = []
                    scaled_reshape = []
                    bb = np.zeros((nrof_faces,4), dtype=np.int32)

                    j = 0
                    for i in range(nrof_faces):
                        emb_array = np.zeros((1, embedding_size))

                        bb[i][0] = det[i][0]
                        bb[i][1] = det[i][1]
                        bb[i][2] = det[i][2]
                        bb[i][3] = det[i][3]

                        # inner exception
                        if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= len(frame[0]) or bb[i][3] >= len(frame):
                            print('face is inner of range!')
                            continue

                        cropped.append(frame[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])

                        cropped[j] = facenet.flip(cropped[j], False)
                        scaled.append(misc.imresize(cropped[j], (image_size, image_size), interp='bilinear'))
                        scaled[j] = cv2.resize(scaled[j], (input_image_size, input_image_size),
                                               interpolation=cv2.INTER_CUBIC)
                        scaled[j] = facenet.prewhiten(scaled[j])
                        scaled_reshape.append(scaled[j].reshape(-1, input_image_size, input_image_size, 3))
                        feed_dict = {images_placeholder: scaled_reshape[j], phase_train_placeholder: False}

                        emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
                        predictions = model.predict_proba(emb_array)
                        best_class_indices = np.argmax(predictions, axis=1)
                        best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
                        cv2.rectangle(frame, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)    #boxing face

                        #plot result idx under box
                        text_x = bb[i][0]
                        text_y = bb[i][3] + 20
                        # print('result: ', best_class_indices[0])

                        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                        faces = faceCascade.detectMultiScale(
                            gray,
                            scaleFactor=1.1,
                            minNeighbors=5,
                            minSize=(2, 2),
                            flags=cv2.CASCADE_SCALE_IMAGE
                        )

                        print("Found {0} faces!".format(len(faces)))

                        # Crop Padding
                        left = 1
                        right = 1
                        top = 1
                        bottom = 1

                        # Draw a rectangle around the faces
                        for (p, y, w, h) in faces:
                            print(p, y, w, h)

                            # Dubugging boxes
                            # cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

                        img = image[y - top:y + h + bottom, p - left:p + w + right]

                        img_data = cv2.resize(img, (227, 227))
                        img = np.reshape(img_data, [-1, 227, 227, 3])
                        img = np.array(img)

                        k = model_1.run(img)
                        detect = np.round(k[0], 3).argmax()
                        print("Detection: ", detect)

                        if detect == 0:

                            for H_i in HumanNames:
                                if HumanNames[best_class_indices[0]] == H_i:
                                    result_names = HumanNames[best_class_indices[0]]
                                    if not best_class_probabilities < 0.8:

                                        cv2.putText(frame, "REAL "+result_names , (text_x, text_y),
                                                    cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                                    1, (0, 0, 255), thickness=1, lineType=2)
                                        print(result_names, best_class_probabilities)

                                        break
                                    cv2.putText(frame, 'REAL but Unknown', (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                                1, (0, 0, 255), thickness=1, lineType=2)

                        if detect == 1:
                            cv2.putText(frame, "INTRUDER", (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                        1, (0, 0, 255), thickness=1, lineType=2)


                        j += 1

                else:
                    print('Unable to align')

            sec = curTime - prevTime
            prevTime = curTime
            fps = 1 / (sec)
            str = 'FPS: %2.3f' % fps
            text_fps_x = len(frame[0]) - 150
            text_fps_y = 20
            cv2.putText(frame, str, (text_fps_x, text_fps_y),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0), thickness=1, lineType=2)
            # c+=1

            # out.write(frame)
            cv2.imshow('Video', frame)
            out.write(frame)


            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        video_capture.release()
        # #video writer
        out.release()
        cv2.destroyAllWindows()

        # video_capture = cv2.VideoCapture(0)
        # c = 0
