from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from flask import Flask, request, render_template, send_from_directory, jsonify


import tensorflow as tf
from scipy import misc
import cv2
import numpy as np
import facenet
import detect_face
import os
import pickle
import json
import time


app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route("/")
def index():
    return render_template("upload1.html")


class ImportGraph():
    """  Importing and running isolated TF graph """
    def __init__(self, loc):
        # Create local graph and use it in the session
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)
        with self.graph.as_default():
            # Import saved model from location 'loc' into local graph
            saver = tf.train.import_meta_graph(loc + '.meta',
                                               clear_devices=True)
            saver.restore(self.sess, loc)
            self.x = self.graph.get_tensor_by_name("Placeholder:0")
            self.hold_prob1 = self.graph.get_tensor_by_name("Placeholder_2:0")
            self.hold_prob2 = self.graph.get_tensor_by_name("Placeholder_3:0")
            self.y_pred = self.graph.get_tensor_by_name("add_7:0")


    def run(self, data):
        """ Running the activation operation previously imported """
        return self.sess.run([tf.nn.softmax(self.y_pred)], feed_dict={self.x: data, self.hold_prob1: 1, self.hold_prob2: 1})



@app.route("/upload", methods=["POST"])
def upload():

    print("POST")
    target = os.path.join(APP_ROOT, 'images/')

    print(target)
    if not os.path.isdir(target):
            os.mkdir(target)
    else:
        print("Couldn't create upload directory: {}".format(target))
    print(request.files.getlist("file"))
    for upload in request.files.getlist("file"):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "/".join([target, filename])
        print ("Accept incoming file:", filename)
        print ("Save it to:", destination)
        print("Destination: ....", destination)
        upload.save(destination)
        img = os.getcwd() + "/images/" + filename
        # print("Image: ", img)

        model_1 = ImportGraph(os.getcwd() + '/models/liveness detection/model.ckpt')
        print("model 1 loaded")

        graph = tf.Graph()
        with graph.as_default():
            with tf.Session(graph=graph) as sess:
                frame_interval = 1
                saver1 = tf.train.import_meta_graph("models/liveness detection/model.ckpt.meta")
                saver1.restore(sess, "models/liveness detection/model.ckpt")
                print("Model restored.")
                print('Initialized')

                cascPath = "haarcascade_frontalface_default.xml"

                # Create the haar cascade
                faceCascade = cv2.CascadeClassifier(cascPath)

                c = 0
                prevTime = 0

                frame = cv2.imread(img)
                image = frame

                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Detect faces in the image
                faces = faceCascade.detectMultiScale(
                    gray,
                    scaleFactor=1.1,
                    minNeighbors=5,
                    minSize=(2, 2),
                    flags=cv2.CASCADE_SCALE_IMAGE
                )

                print("Found {0} faces!".format(len(faces)))

                # Crop Padding
                left = 1
                right = 1
                top = 1
                bottom = 1

                # Draw a rectangle around the faces
                for (p, y, w, h) in faces:
                    print(p, y, w, h)

                    # Dubugging boxes
                    cv2.rectangle(frame, (p, y), (p + w, p + h), (0, 255, 0), 2)

                img = frame[y - top:y + h + bottom, p - left:p + w + right]

                img_data = cv2.resize(img, (227, 227))
                img = np.reshape(img_data, [-1, 227, 227, 3])
                img = np.array(img)
                # print(img)

                # a = []
                k = model_1.run(img)
                a = k[0]
                b = a[0]

                r = np.round(k[0], 3).argmax()

                if r == 0:
                    face = "REAL"

                if r == 1:
                    face = "INTRUDER"

                return jsonify(face=face)



@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("images", filename)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5566, debug=True)
