from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from flask import Flask, request, render_template, send_from_directory, jsonify


import tensorflow as tf
from scipy import misc
import cv2
import numpy as np
import facenet
import detect_face
import os
import pickle
import json
import time
import datetime


app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route("/")
def index():
    return render_template("upload1.html")

class ImportGraph():
    """  Importing and running isolated TF graph """
    def __init__(self, loc):
        # Create local graph and use it in the session
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)
        with self.graph.as_default():
            # Import saved model from location 'loc' into local graph
            saver = tf.train.import_meta_graph(loc + '.meta',
                                               clear_devices=True)
            saver.restore(self.sess, loc)
            self.x = self.graph.get_tensor_by_name("Placeholder:0")
            self.hold_prob1 = self.graph.get_tensor_by_name("Placeholder_2:0")
            self.hold_prob2 = self.graph.get_tensor_by_name("Placeholder_3:0")
            self.y_pred = self.graph.get_tensor_by_name("add_7:0")


    def run(self, data):
        """ Running the activation operation previously imported """
        return self.sess.run([tf.nn.softmax(self.y_pred)], feed_dict={self.x: data, self.hold_prob1: 1, self.hold_prob2: 1})



@app.route("/upload", methods=["GET","POST"])
def upload():
    cascPath = "haarcascade_frontalface_default.xml"

    # Create the haar cascade
    faceCascade = cv2.CascadeClassifier(cascPath)

    for upload in request.files.getlist("file"):

        img_array = np.array(bytearray(upload.read()), dtype=np.uint8)
        frame = cv2.imdecode(img_array, -1)

        print('Creating networks and loading parameters')
        with tf.Graph().as_default():
            gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
            sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
            with sess.as_default():
                model_1 = ImportGraph(os.getcwd() + '/models/liveness detection/model.ckpt')
                pnet, rnet, onet = detect_face.create_mtcnn(sess, os.getcwd() + '/align')

                minsize = 40  # minimum size of face
                threshold = [0.6, 0.8, 0.92]  # three steps's threshold
                factor = 0.709  # scale factor
                margin = 44
                frame_interval = 3
                batch_size = 100
                image_size = 182
                input_image_size = 160

                currTime = time.time()
                with open('data.txt') as json_file:
                    HumanNames = json.load(json_file)

                presentTime = time.time()
                print("Loading time ", presentTime - currTime)

                print('Loading feature extraction model')


                modeldir = os.getcwd() + '/models/facenet/20170512-110547.pb'
                facenet.load_model(modeldir)


                images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
                embedding_size = embeddings.get_shape()[1]

                classifier_filename = os.getcwd() + '/models/classifier/classifier.pkl'
                classifier_filename_exp = os.path.expanduser(classifier_filename)
                with open(classifier_filename_exp, 'rb') as infile:
                    (model, class_names) = pickle.load(infile)
                    print('load classifier file-> %s' % classifier_filename_exp)

                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Detect faces in the image
                faces = faceCascade.detectMultiScale(
                    gray,
                    scaleFactor=1.1,
                    minNeighbors=5,
                    minSize=(2, 2),
                    flags=cv2.CASCADE_SCALE_IMAGE
                )

                print("Found {0} faces!".format(len(faces)))

                # Crop Padding
                left = 1
                right = 1
                top = 1
                bottom = 1

                # Draw a rectangle around the faces
                for (p, y, w, h) in faces:
                    print(p, y, w, h)

                    # Dubugging boxes
                    cv2.rectangle(frame, (p, y), (p + w, p + h), (0, 255, 0), 2)

                img = frame[y - top:y + h + bottom, p - left:p + w + right]

                img_data = cv2.resize(img, (227, 227))
                img = np.reshape(img_data, [-1, 227, 227, 3])
                img = np.array(img)

                k = model_1.run(img)
                a = k[0]
                b = a[0]

                detection = np.round(k[0], 3).argmax()

                print('Start Recognition!')
                prevTime = 0
                c = 0

                curTime = time.time() + 1  # calc fps
                timeF = frame_interval

                if (c % timeF == 0):
                    find_results = []

                    if frame.ndim == 2:
                        frame = facenet.to_rgb(frame)

                    frame = frame[:, :, 0:3]
                    bounding_boxes, _ = detect_face.detect_face(frame, minsize, pnet, rnet, onet, threshold, factor)
                    nrof_faces = bounding_boxes.shape[0]
                    print('Face Detected: %d' % nrof_faces)

                    if nrof_faces > 0:
                        det = bounding_boxes[:, 0:4]
                        img_size = np.asarray(frame.shape)[0:2]

                        cropped = []
                        scaled = []
                        scaled_reshape = []
                        bb = np.zeros((nrof_faces, 4), dtype=np.int32)

                        # j = 0
                        for i in range(nrof_faces):
                            emb_array = np.zeros((1, embedding_size))

                            bb[i][0] = det[i][0]
                            bb[i][1] = det[i][1]
                            bb[i][2] = det[i][2]
                            bb[i][3] = det[i][3]

                            # inner exception
                            if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= len(frame[0]) or bb[i][3] >= len(frame):
                                print('face is too close')
                                continue

                            cropped.append(frame[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])
                            cropped[i] = facenet.flip(cropped[i], False)
                            scaled.append(misc.imresize(cropped[i], (image_size, image_size), interp='bilinear'))
                            scaled[i] = cv2.resize(scaled[i], (input_image_size, input_image_size),
                                                   interpolation=cv2.INTER_CUBIC)
                            scaled[i] = facenet.prewhiten(scaled[i])
                            scaled_reshape.append(scaled[i].reshape(-1, input_image_size, input_image_size, 3))
                            feed_dict = {images_placeholder: scaled_reshape[i], phase_train_placeholder: False}
                            emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
                            predictions = model.predict_proba(emb_array)

                            # print(predictions)
                            best_class_indices = np.argmax(predictions, axis=1)
                            # print(best_class_indices)
                            best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]


                            if detection == 0:

                                for H_i in HumanNames:
                                    # print(H_i)
                                    if HumanNames[best_class_indices[0]] == H_i:
                                        result_names = HumanNames[best_class_indices[0]]
                                        if best_class_probabilities >= 0.8:
                                            print(result_names)
                                            print(best_class_probabilities)
                                            name  = result_names
                                            id = best_class_indices[0].astype(np.str)
                                        else:
                                            name = "Unknown"
                                            id = None
                            if detection == 1:
                                name = "INTRUDER"
                                id = None
                    else:
                        print('Unable to align')
                        name = None
                        id = None

    now = datetime.datetime.now()
    checkin_time = now.strftime("%H:%M:%S")
    print(now.strftime("%H:%M:%S"))

    return jsonify(name=name,time=checkin_time)

@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("images", filename)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5566, debug=True)
